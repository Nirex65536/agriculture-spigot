package nirex.nirex;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RandomTicker {

    public RandomTicker(int Speed) {
        Config config = Main.getInstance().getConfiguration();
        Random random = new Random();

        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), () -> Bukkit.getWorlds().forEach((world) -> Arrays.asList(world.getLoadedChunks()).forEach((chunk) -> {
            Block target = chunk.getBlock(random.nextInt(0, 15),
                    random.nextInt(0, 256), random.nextInt(0, 15));
            if(Listener.checkSpawn(target)) return;
            if(Arrays.asList(new Material[]{Material.OAK_LEAVES,
                Material.AZALEA_LEAVES, Material.FLOWERING_AZALEA_LEAVES}).contains(target.getType())) {

                if(!config.getConfig().getBoolean("apple-dropping")) return;
                target.getWorld().dropItem(target.getLocation(), new ItemStack(Material.APPLE));
            }
            else if(Arrays.asList(new Material[]{Material.GRASS, Material.TALL_GRASS, Material.FERN, Material.LARGE_FERN})
                    .contains(target.getType())) {
                if(!config.getConfig().getBoolean("seed-dropping")) return;
                target.getWorld().dropItem(target.getLocation(), new ItemStack(Material.WHEAT_SEEDS));
            }
            else if(Arrays.asList(new Material[]{Material.GRASS_BLOCK, Material.DIRT, Material.RED_SAND, Material.SAND}).contains(target.getType())) {
                if(!config.getConfig().getBoolean("sapling-drought")) return;
                for(int x = -3; x <= 3; x++) {
                    for(int y = -3; y <= 3; y++) {
                        for(int z = -3; z <= 3; z++) {
                            if(!Arrays.asList(new Biome[]{Biome.BADLANDS, Biome.WOODED_BADLANDS,
                                    Biome.SAVANNA, Biome.DESERT, Biome.SAVANNA_PLATEAU, Biome.WINDSWEPT_SAVANNA, Biome.NETHER_WASTES,
                                    Biome.CRIMSON_FOREST, Biome.WARPED_FOREST, Biome.BASALT_DELTAS, Biome.SOUL_SAND_VALLEY,
                                    Biome.ERODED_BADLANDS}).contains(target.getRelative(x,y,z).getBiome())) continue;
                            if(!Arrays.asList(new Material[]{Material.ACACIA_SAPLING, Material.JUNGLE_SAPLING,
                                    Material.OAK_SAPLING, Material.DARK_OAK_SAPLING, Material.BIRCH_SAPLING,
                                    Material.SPRUCE_SAPLING, Material.MANGROVE_PROPAGULE, Material.AZALEA,
                                    Material.FLOWERING_AZALEA}).contains(target.getRelative(x,y,z).getType())) continue;

                            target.getRelative(x,y,z).setType(Material.DEAD_BUSH);
                        }
                    }
                }
            }
            else if(Arrays.asList(new Material[]{Material.GRASS_BLOCK, Material.DIRT, Material.PODZOL}).contains(target.getType())) {
                if(!config.getConfig().getBoolean("flower-spreading")) return;
                Block realTarget = target.getRelative(BlockFace.UP);
                if(!(realTarget.getType().equals(Material.AIR) || realTarget.getType().equals(Material.SNOW))) return;
                if(!(realTarget.getLightLevel() > 5)) return;
                int i = config.getConfig().getInt("flower-spreading-radius");
                List<Block> nearbyFlowers = new ArrayList<>();
                for(int x = -i; x <= i; x++) {
                    for(int y = -i; y <= i; y++) {
                        for(int z = -i; z <= i; z++) {
                            if(Arrays.asList(new Material[]{Material.DANDELION, Material.POPPY, Material.BLUE_ORCHID,
                                Material.ALLIUM, Material.AZURE_BLUET, Material.RED_TULIP, Material.ORANGE_TULIP,
                                Material.WHITE_TULIP, Material.PINK_TULIP, Material.OXEYE_DAISY, Material.LILY_OF_THE_VALLEY,
                                Material.CORNFLOWER}).contains(target.getRelative(x, y, z).getType())) nearbyFlowers.add(target.getRelative(x, y, z));
                        }
                    }
                }
                Random random1 = new Random();
                if(nearbyFlowers.isEmpty()) return;
                realTarget.setType(nearbyFlowers.get(random1.nextInt(0, nearbyFlowers.size())).getType());
            }
        })), 1, Speed);
    }
}
