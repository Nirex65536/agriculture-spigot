package nirex.nirex;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

public class Config {
    private final File file;

    private final YamlConfiguration config;

    public Config() {
        File dir = new File("./plugins/agriculture");
        if (!dir.exists())
            dir.mkdirs();
        this.file = new File(dir, "config.yml");
        if (!this.file.exists())
            try {
                this.file.createNewFile();
            } catch (IOException ignored) {}
        new YamlConfiguration();
        this.config = YamlConfiguration.loadConfiguration(this.file);
    }

    public YamlConfiguration getConfig() {
        return this.config;
    }

    public void save() {
        try {
            this.config.save(this.file);
        } catch (IOException ignored) {}
    }

    public void checkDefaults() {
        if(this.config.getBoolean("keep-configuration")) return;
        Bukkit.getLogger().log(Level.WARNING, "Resetting agriculture config");
        config.set("keep-configuration", true);
        config.set("spawn.south", 0);
        config.set("spawn.east", 0);
        config.set("spawn.north", 0);
        config.set("spawn.west", 0);
        config.set("apple-dropping", true);
        config.set("seed-dropping", true);
        config.set("sapling-drought", true);
        config.set("flower-spreading", true);
        config.set("flower-spreading-radius", 5);
        config.set("tick-speed", 10);
        config.set("decay.golden-apple-chance", 0);
        config.set("decay.god-apple-chance", 0);
        config.set("decay.leave-chance", 5);
        config.set("decay.dead-bush-chance", 5);
        config.set("decay.bonemeal-chance", 20);
        config.set("tnt-block-knockback", true);
        save();
    }
}
