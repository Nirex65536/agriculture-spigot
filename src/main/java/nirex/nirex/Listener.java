package nirex.nirex;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;

import java.util.*;

public class Listener implements org.bukkit.event.Listener {
    private static Player player;
    private static Block target;
    private static Material targetMaterial;
    private static ItemStack mainHand;

    public static boolean checkSpawn(Block b) {
        Config config = Main.getInstance().getConfiguration();
        Location l = b.getLocation();
        return (l.getBlockZ() < config.getConfig().getInt("spawn.south") &&
                l.getBlockZ() > config.getConfig().getInt("spawn.north")) &&
                (l.getBlockX() < config.getConfig().getInt("spawn.east") &&
                        l.getBlockX() > config.getConfig().getInt("spawn.west"));
    }

    private static void runCrops() {
        Crops crop = (Crops)target.getState().getData();
        if(!crop.getState().equals(CropState.RIPE)) return;

        target.getDrops(mainHand).forEach((drop) -> target.getWorld().dropItem(target.getLocation().add(0, 1, 0), drop));

        crop.setState(CropState.SEEDED);
        target.getState().setData(crop);
        target.setType(target.getType());
        player.playSound(player.getLocation(), Sound.BLOCK_CROP_BREAK, 100, 1);
        player.playSound(player.getLocation(), Sound.ITEM_CROP_PLANT, 100, 1);
    }

    private static void runPlants() {
        for (double y = 0; target.getLocation().add(0, y, 0).getBlock().getType().equals(targetMaterial); y--)
        for (double i = y + 1; target.getLocation().add(0, i, 0).getBlock().getType().equals(targetMaterial); i++) {
            final Block temp = target.getLocation().add(0, i, 0).getBlock();
            temp.getDrops(mainHand).forEach((drop) -> target.getWorld().dropItem(temp.getLocation(), drop));
            temp.setType(Material.AIR);
        }
    }

    private static void getConnectedblocks(Block block, Set<Block> results, List<Block> todo) {
        for (BlockFace face : BlockFace.values()) {
            Block b = block.getRelative(face);
            if (b.getType() == block.getType()) {
                if (results.add(b)) {
                    todo.add(b);
                }
            }
        }
    }

    public static Set<Block> getNearbyBlocks(Block block) {
        Set<Block> set = new HashSet<>();
        LinkedList<Block> list = new LinkedList<>();
        list.add(block);
        while((block = list.poll()) != null) {
            getConnectedblocks(block, set, list);
        }
        return set;
    }

    private static void runTimber() {
        List<Material> Axes = Arrays.asList(Material.WOODEN_AXE,
                Material.STONE_AXE,
                Material.IRON_AXE,
                Material.GOLDEN_AXE,
                Material.DIAMOND_AXE,
                Material.NETHERITE_AXE);

        getNearbyBlocks(target).forEach((block) -> {
            if(checkSpawn(block) && !player.hasPermission("agriculture.spawnedit")) return;
            if(!Axes.contains(mainHand.getType())) return;
            mainHand.setDurability((short)(mainHand.getDurability()+1));
            if(mainHand.getDurability() >= mainHand.getType().getMaxDurability()) {
                player.playSound(player.getLocation(), Sound.ITEM_SHIELD_BREAK,100f, 0.8f);
                Objects.requireNonNull(player.getEquipment()).setItemInMainHand(new ItemStack(Material.AIR));
                return;
            }
            block.getDrops(mainHand).forEach((drop) -> target.getWorld().dropItem(block.getLocation(), drop));
            block.setType(Material.AIR);
        });
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            player = e.getPlayer();
            if(player.isSneaking()) return;
            target = e.getClickedBlock();
            assert target != null;
            if(checkSpawn(target) && !player.hasPermission("agriculture.spawnedit")) return;
            targetMaterial = target.getType();
            mainHand = player.getInventory().getItemInMainHand();
            if (!player.hasPermission("agriculture.use." + targetMaterial.toString().toLowerCase())) return;
            switch (targetMaterial) {
                case WHEAT:
                case CARROTS:
                case POTATOES:
                case BEETROOTS: {
                    runCrops();
                    break;
                }
                case BIG_DRIPLEAF:
                case TWISTING_VINES:
                case BAMBOO:
                case CACTUS:
                case KELP_PLANT:
                case SUGAR_CANE: {
                    runPlants();
                    break;
                }
                case CRIMSON_STEM:
                case WARPED_STEM:
                case DARK_OAK_LOG:
                case MANGROVE_LOG:
                case JUNGLE_LOG:
                case ACACIA_LOG:
                case SPRUCE_LOG:
                case BIRCH_LOG:
                case OAK_LOG: {
                    runTimber();
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onLeaveDecay(LeavesDecayEvent e) {
        if(checkSpawn(e.getBlock())) return;
        Config config = Main.getInstance().getConfiguration();
        Random r = new Random();
        if(r.nextInt(0, 400) < config.getConfig().getInt("decay.golden-apple-chance"))
            e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(Material.GOLDEN_APPLE));
        if(r.nextInt(0, 400) < config.getConfig().getInt("decay.god-apple-chance"))
            e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(Material.ENCHANTED_GOLDEN_APPLE));
        if(r.nextInt(0, 400) < config.getConfig().getInt("decay.leave-chance"))
            e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(e.getBlock().getType()));
        if(r.nextInt(0, 400) < config.getConfig().getInt("decay.dead-bush-chance"))
            e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(Material.DEAD_BUSH));
        if(r.nextInt(0, 400) < config.getConfig().getInt("decay.bonemeal-chance"))
            e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(Material.BONE_MEAL));
    }

    private static void handleExplosion(Location location, List<Block> blockList, boolean fire) {
        List<Material> blacklisted = Arrays.asList(Material.SPAWNER,
                Material.BEDROCK,
                Material.FIRE,
                Material.POWDER_SNOW,
                Material.STRUCTURE_VOID,
                Material.BLACK_BED,
                Material.BLUE_BED,
                Material.BROWN_BED,
                Material.GREEN_BED,
                Material.CYAN_BED,
                Material.GRAY_BED,
                Material.YELLOW_BED,
                Material.PINK_BED,
                Material.PURPLE_BED,
                Material.LIME_BED,
                Material.LIGHT_BLUE_BED,
                Material.LIGHT_GRAY_BED,
                Material.MAGENTA_BED,
                Material.ORANGE_BED,
                Material.WHITE_BED,
                Material.RED_BED,
                Material.TNT,
                Material.DRAGON_EGG);
        blockList.forEach((block) -> {
            if(blacklisted.contains(block.getType())) return;
            if((new Random().nextInt(0, 10) <= 3) && fire) {
                block.setType(Material.FIRE);
            }
            FallingBlock falling = block.getWorld().spawnFallingBlock(block.getLocation().add(0.5, 2, 0.5), block.getType().createBlockData());
            falling.setVelocity((location.toVector().subtract(falling.getLocation().toVector())).normalize().multiply(-0.75));
        });
    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        if(!Main.getInstance().getConfiguration().getConfig().getBoolean("tnt-block-knockback")) return;
        handleExplosion(e.getEntity().getLocation(), e.blockList(),false);
    }

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e) {
        if(!Main.getInstance().getConfiguration().getConfig().getBoolean("tnt-block-knockback")) return;
        handleExplosion(e.getBlock().getLocation(), e.blockList(), true);
    }
}
