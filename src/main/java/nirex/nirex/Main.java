package nirex.nirex;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {
    private static Main instance;
    private Config config;

    @Override
    public void onLoad() {
        instance = this;
        config = new Config();
        config.checkDefaults();
    }
    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new Listener(), this);
        new RandomTicker(config.getConfig().getInt("tick-speed"));
    }

    @Override
    public void onDisable() {
    }

    public static Main getInstance() {
        return instance;
    }

    public Config getConfiguration() {
        return config;
    }
}
