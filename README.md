# Spigot-Agriculture
This is a spigot plugin that adds some completely random features to Minecraft. (Spigot 1.19.X, may also work with newer versions)

## Auto-Harvesting
Auto-Harvesting is supported for:
 - Sugar cane
 - Bamboo
 - Kelp
 - Cacti
 - Twisting vines (just don't ask why)
 - Big dripleaves
 - Wheat
 - Carrots
 - Potatoes
 - Beetroots
 - All kinds of logs (oak, birch, spruce, dark-oak, jungle, mangrove, acacia, crimson-stem, warped-stem)

To use auto-harvesting, the player in question must have the necessary permissions. To manage permissions I recommend the [LuckPerms](https://github.com/LuckPerms/LuckPerms) plugin. The following permissions can be granted:
 - agriculture.use.acacia_log
 - agriculture.use.bamboo
 - agriculture.use.beetroots
 - agriculture.use.birch_log
 - agriculture.use.cactus
 - agriculture.use.carrots
 - agriculture.use.crimson_stem
 - agriculture.use.dark_oak_log
 - agriculture.use.jungle_log
 - agriculture.use.kelp_plant
 - agriculture.use.mangrove_log
 - agriculture.use.oak_log
 - agriculture.use.potatoes
 - agriculture.use.spruce_log
 - agriculture.use.sugar_cane
 - agriculture.use.warped_stem
 - agriculture.use.wheat

To trigger the plugin, the block in question must be right-clicked. For any type of wood, an axe is needed for this. This will also lose durability when a tree is chopped. 

## Configuration
```yml
keep-configuration: true
```
When set to false, the configuration will be reset to the defaults the next time the server reloads.


```yml
spawn:
  south: 0
  east: 0
  north: 0
  west: 0
```
The specified area is considered spawn protection and is not processed by the plugin

```yml
apple-dropping: true
```
When set to true, apples will randomly drop from oak-, azalea- and flowering-azalea-leaves.
***This may create lag when too many forest-chunks are loaded***

```yml
seed-dropping: true
```
When set to true, wheat seeds will randomly drop from grass.
***This could lead to lags with high player counts***

```yml
sapling-drought: true
```
When set to true, saplings have a chance to convert to dead bushes in the following biomes:
- BADLANDS
- BASALT_DELTAS
- CRIMSON_FOREST
- ERODED_BADLANDS
- NETHER_WASTES
- SAVANNA
- SAVANNA_PLATEAU
- SOUL_SAND_VALLEY
- WARPED_FOREST
- WINDSWEPT_SAVANNA
- WOODED_BADLANDS

```yml
flower-spreading: true
```
When set to true, flowers will spread naturally.

```yml
flower-spreading-radius: 5
```
The radius in which flowers can spread.

```yml
tick-speed: 10
```
The speed for the internal random-tick-system. 10 means every 10th tick, 1 would mean every tick.

```yml
decay:
  golden-apple-chance: 0
  god-apple-chance: 0
  leave-chance: 5
  dead-bush-chance: 5
  bonemeal-chance: 20
```
When a leaf rots it has the following chance in 400 (it's not in percent because I barely slept the two nights before I wrote this and was completely overtired) to drop the specified item.

```yml
tnt-block-knockback: true
```
The only good feature this plugin has to provide. When set to true, explosions will summon falling blocks to create a nice little animation.

## Compilation
Compile using Intellij-Idea
